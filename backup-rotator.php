<?php
/**
 * Created by PhpStorm.
 * User: lera
 * Date: 23.06.16
 * Time: 14:15
 */
/** --path=/full path to the file folder
 * This command --keep = the number of input files
 * $dir - directory path
 * $filles - list of files that are located in the specified path
 */
$command = array();
$path = '';
$coutnfile = 0;
if (isset($argv[1])) { //Here we used a system variable argc to obtain the number of all parameters


    //Here we take command arguments in variables
    $longopts  = array(
        "path:",
        "keep:",
    );

    /*
     *Here each element of the array compared with the parameters passed to the script, which will stand in front of the (--).
     *Remakes commands --path and --keep the array ['path'=>'path ', 'keep'=>5]
     */
    $commands = getopt('', $longopts);
    $path = $commands['path'];
    $coutnfile = $commands['keep'];

    $dir = $path;
    $files = array();
    $filles = scandir($dir);//list of files that are located in the specified path. Function scandir

    /* iterate through each file in turn $filles file from the list and write down the full path of the file in an array of files
     * do not consider the file entry "link on the current directory" and "reference to the current directory"
     * */
    foreach ( $filles as $file) {
        if($file == '.' || $file == '..'){continue;}
        $files[] = "$dir/$file";
    };

    /**
     * compares the elements of the array files by date of creation
     */
    usort($files, function ($a, $b) {
        if (filemtime($a) == filemtime($b)) return 0;//if the files are not recorded
        return (filemtime($a) < filemtime($b)) ? -1 : 1;//if the file date is greater than another file that records the date back to the array of $files
    });
    $toDelete = count($files) - $coutnfile;//We count the number of files - the number of files that the user has entered = number of deleted files
    $deleted = array();

    /**
     *  I delete all the files until I will go to --keep
     * for sort out number of files that we want to remove the $ toDelete, delete files and files array element corresponding to this file
     */
    for ($i = 0; $i < $toDelete; $i++) {
        unlink($files[$i]);
        $deleted[] = $files[$i] . ' - ' . 'DELETED!';
        unset($files[$i]);
    }
    print(implode("\r\n", $deleted));// with an array of line and doing derive deleted files
    print("\r\n");
    print_r($files);//список файлов которые остались в папке

}

