Developed by Skynix

This is a simple php script that can be run from shell in the following way:
php backup-rotator.php --path=/path/to/folder/withbackups/ --keep=5

where 
--path is the argument with the path where backups are stored
--keep is the argument with the number of backups that we should keep (the rest of backups we should delete)

Backup files looks like:
backup-2016_06_12.zip
backup-2016_06_14.zip
backup-2016_06_16.zip
backup-2016_06_18.zip
backup-2016_06_20.zip
backup-2016_06_22.zip
backup-2016_06_13.zip
backup-2016_06_15.zip
backup-2016_06_17.zip
backup-2016_06_19.zip
backup-2016_06_21.zip
backup-2016_06_23.zip

The logic

* The script reads a list of files in the folder 
* The script parse dates from file names
* The script sorts an array with files by date
* The script goes in a loop through the array of backups and delete the oldest files until there are {--keep} the most fresh backups left
